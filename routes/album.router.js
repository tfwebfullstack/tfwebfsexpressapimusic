const albumController = require('../controllers/album.controller');
const authJwt = require('../middlewares/auth.jwt.middleware');
const bodyValidation = require('../middlewares/body.validator');
const pagination = require('../middlewares/pagination.middleware');
const {albumValidator, albumCoverValidator } = require('../validators/album.validators');

const albumRouter = require('express').Router()


// -------- CONFIG MULTER  ----------
const multer = require('multer')
const storage = require('../utils/config.multer')('covers')
const upload = multer({ storage })

// -------- FIN CONFIG MULTER -------

albumRouter.route('/')
    .get(pagination(), albumController.getAll)
    .post(bodyValidation(albumValidator), albumController.create)
    //.post(authJwt(["Admin"]), bodyValidation(albumValidator), albumController.create)


albumRouter.route('/:id')
    .get(albumController.getById)
    .put(bodyValidation(albumValidator), albumController.update)
    //.put(authJwt(["Admin"]), bodyValidation(albumValidator), albumController.update)
    //Attention : la validation du body, toujurs avant le middleware multer !
    //Comme cela, on éviter d'upload l'image dans le dossier public alors que les infos sont peut-être éronnées
    .patch(bodyValidation(albumCoverValidator), upload.single('cover') , albumController.updateCover)
    .delete(albumController.delete)
    //.delete(authJwt(["Admin"]), albumController.delete)


module.exports = albumRouter;