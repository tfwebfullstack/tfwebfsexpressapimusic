const userController = require('../controllers/user.controller');
const authJwt = require('../middlewares/auth.jwt.middleware');
const bodyValidation = require('../middlewares/body.validator');
const pagination = require('../middlewares/pagination.middleware');
const updateUserValidator = require('../validators/user.validators');

const userRouter = require('express').Router();

// -------- CONFIG MULTER  ----------
const multer = require('multer')
const storage = require('../utils/config.multer')('avatars')
const upload = multer({ storage })

userRouter.route('/')
    // .get(pagination(), userController.getAll)
    .get(authJwt(), pagination(), userController.getAll)

userRouter.route('/:id')
    //.get(userController.getById)
    .get(authJwt(), userController.getById)
    //TODO : Dans le GetById, vérifier si Admin, si pas, vérifier id similaires
    //.put(bodyValidation(updateUserValidator), userController.update)
    .put(authJwt(), bodyValidation(updateUserValidator), userController.update)
    //TODO : Dans le Update, vérifier si Admin, si pas, vérifier id similaires
    //.delete(userController.delete)
    .patch(authJwt(), upload.single('avatar'), userController.updateAvatar)
    .delete(authJwt(["Admin"]) ,userController.delete)


module.exports = userRouter;