const trackController = require('../controllers/track.controller');
const authJwt = require('../middlewares/auth.jwt.middleware');
const bodyValidation = require('../middlewares/body.validator');
const pagination = require('../middlewares/pagination.middleware');
const {createTrackValidator, updateTrackValidator} = require('../validators/track.validators');

const trackRouter = require('express').Router()

trackRouter.route('/')
    .get(pagination( {defaultLimit : 10} ), trackController.getAll)
    // .post( bodyValidation(createTrackValidator) ,trackController.create)
    .post(authJwt(["Admin"]), bodyValidation(createTrackValidator) ,trackController.create)

trackRouter.route('/:id')
    .get(trackController.getById)
    // .put(bodyValidation(updateTrackValidator), trackController.update)
    .put(authJwt(["Admin"]), bodyValidation(updateTrackValidator), trackController.update)
    // .delete(trackController.delete)
    .delete(authJwt(["Admin"]), trackController.delete)

trackRouter.route('/:id/like')
    .post( authJwt() , trackController.like)

trackRouter.route('/:id/dislike')
    .delete( authJwt(), trackController.dislike)

module.exports = trackRouter;