const yup = require('yup')

const albumValidator = yup.object({
    title : yup.string().required().trim().max(50),
    cover : yup.string().trim().nullable()
})

const albumCoverValidator = yup.object().shape({
    cover : yup.mixed()/*.required("Image requise")*/
})

module.exports = { albumValidator, albumCoverValidator }