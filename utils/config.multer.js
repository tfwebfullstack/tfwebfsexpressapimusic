// -------- CONFIG MULTER  ----------
// Import de multer
const multer = require('multer')
// Import de uuid
const uuid = require('uuid')


const config = (folder) => {
     
    // Basique
        //On choisit juste la destination, et multer se charge du nom de fichier etc
        //const upload = multer({ dest : 'public/images/covers'})

    // Pimpée
        //On choisit la destination et on se charge du nom du fichier etc
    return storage = multer.diskStorage({
        //configuration de la destination
        destination : (req, file, callback) => {
            callback(null, `public/images/${folder}`)
        },
        //configuration du nom de fichier
        filename : (req, file, callback) => {
            console.log("multer file : ", file);
            //Création du nom de fichier
            // Générer un uuid
            const name = uuid.v4()
            //Récupération de l'extension
            //On découpe le nom du fichier ex Sopico.jpg via les . avec split
            //on récupère ensuite, la dernière case du tableau, qui contient l'extension
            const ext = file.originalname.split('.').at(-1) //Ma.Super.Cover.jpg
            //Renvoi du nom du fichier ainsi créé
            callback(null, name + '.' + ext)
        }
    })
}

module.exports = config
