const { UserDTO } = require("../dto/user.dto");
const db = require("../models");

const userService = {
    getAll : async (offset, limit) => {
        const { rows, count } = await db.User.findAndCountAll({
           distinct : true,
           offset,
           limit 
        });

        return {
            users : rows.map(user => new UserDTO(user)),
            count
        }

    },

    getById : async (id) => {
       const user = await db.User.findByPk(id);
       return user ? new UserDTO(user) : null;

    },

    update : async (userToUpdate, id) => {
        const updatedRow = await db.User.update(userToUpdate, {
            where : { id }
        });
        return updatedRow[0] === 1;
    },

    updateAvatar : async (id, filename) => {
        const data = {
            avatar : `/images/avatars/${filename}`
        }

        const updatedRow = await db.User.update(data, {
            where : { id }
        })

        //Première case du tableau : nombre de lignes modifiées
        return updatedRow[0] === 1 //On regarde si c'est un égal à 1 : une ligne a bien été modifiée
    },

    delete : async (id) => {
        const nbDeletedRow = await db.User.destroy({
            where : { id }
        });

        return nbDeletedRow === 1;
    }
}

module.exports = userService;