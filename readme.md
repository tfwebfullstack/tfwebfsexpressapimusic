# Recap étapes

## Initialisation du projet
- créer un fichier app.js
- npm init → créer le package.json avec les infos projet + dépendances

## Ajout des premières dépendances (lib)
### Outils de dev
- nodemon
### WebServer Express
- express
- express-async-errors
### Gestion des variables d'environnement
- dotenv
### DB (Sql Server)
- sequelize
- tedious

## Créer le fichier gitignore
- ignorer node_modules + fichiers env
- si extension gitignore installée → Ctrl + Maj + P (F1) → add gitignore -> node

## Ajouter dans les scripts :
- "dev" : "nodemon app.js"
## Créer l'architecture de base du projet + Créer le fichier .env
ExpressAPIMusic  <br>
├── controllers/  <br> 
├── dto/  <br> 
├── middlewares/  <br> 
├── models/  <br> 
├── routes/  <br> 
├── services/  <br> 
├── .env  <br> 
├── .gitignore  <br>
├── app.js  <br> 
├── package-lock.json  <br> 
├── package.json  <br> 

## Mise en place des controllers et des routers
ExpressAPIMusic  <br> 
├── controllers/  <br> 
&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;├── album.controller.js  <br> 
&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;├── artist.controller.js  <br> 
&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;├── genre.controller.js  <br> 
&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;├── track.controller.js  <br> 
├── dto/  <br> 
├── middlewares/  <br> 
├── models/  <br> 
├── routes/  <br> 
&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;├── album.router.js  <br> 
&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;├── artist.router.js  <br> 
&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;├── genre.router.js  <br> 
&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;├── index.js  <br> 
&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;├── track.router.js  <br> 
├── services/  <br> 
├── .env  <br> 
├── .gitignore  <br> 
├── app.js  <br> 
├── package-lock.json  <br> 
├── package.json  <br> 

## Ajout du router dans l'app

## Création de la DB sur SSMS + Attribution des droits à notre User

## Ajouter dans le .env les infos DB

## Setup DB
- Création du index.js avec instance de sequelize et objet db + liaison modèle + relations
- Création de tous les modèles
- Nouvelle arborescence
ExpressAPIMusic  <br> 
├── controllers/  <br> 
&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;├── album.controller.js  <br> 
&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;├── artist.controller.js  <br> 
&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;├── genre.controller.js  <br> 
&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;├── track.controller.js  <br> 
├── dto/  <br> 
├── middlewares/  <br> 
├── models/  <br>
&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;├── album.model.js  <br> 
&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;├── artist.model.js  <br> 
&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;├── genre.model.js  <br> 
&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;├── index.js  <br> 
&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;├── track.model.js  <br>
&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;├── mm_artist_track.model.js  <br> 
├── routes/  <br> 
&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;├── album.router.js  <br> 
&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;├── artist.router.js  <br> 
&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;├── genre.router.js  <br> 
&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;├── index.js  <br> 
&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;├── track.router.js  <br> 
├── services/  <br> 
├── .env  <br> 
├── .gitignore  <br> 
├── app.js  <br> 
├── package-lock.json  <br> 
├── package.json  <br> 

- Connection db + synchro dans app.js

## Création des Services, DTO et méthodes controller
- Création du index.js avec instance de sequelize et objet db + liaison modèle + relations
- Création de tous les modèles
- Nouvelle arborescence<br>
ExpressAPIMusic  <br> 
├── controllers/  <br> 
&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;├── album.controller.js  <br> 
&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;├── artist.controller.js  <br> 
&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;├── genre.controller.js  <br> 
&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;├── track.controller.js  <br> 
├── dto/ <br>
&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;├── genre.dto.js  <br>  
├── middlewares/  <br> 
├── models/  <br>
&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;├── album.model.js  <br> 
&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;├── artist.model.js  <br> 
&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;├── genre.model.js  <br> 
&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;├── index.js  <br> 
&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;├── track.model.js  <br>
&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;├── mm_artist_track.model.js  <br> 
├── routes/  <br> 
&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;├── album.router.js  <br> 
&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;├── artist.router.js  <br> 
&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;├── genre.router.js  <br> 
&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;├── index.js  <br> 
&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;├── track.router.js  <br> 
├── services/  <br>
&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;├── genre.service.js  <br> 
├── .env  <br> 
├── .gitignore  <br> 
├── app.js  <br> 
├── package-lock.json  <br> 
├── package.json  <br> 

## Ajout de l'utils SuccessResponse
- Permet d'envoyer comme response, le resultat, le count (si tableau) et le statusCode

## Ajout d'une pagination
- Dans un controller (pour comprendre comment ça fonctionne)
- Mise en place d'un middleware pour la pagination

## Mise en place User
- Création model et lien Many to Many
- Création service + DTO
- Création UserController (GetAll, GetById, Update, Delete) et AuthController (Register, Login)
- Création routes
- Hashage Password et Verif Password au login (npm i argon2)

## Rework des Tracks 
### Insertion 
- Rajouter le genre
- Rajouter les albums qui lui sont liés
- Rajouter les artists qui lui sont liés (en featuring ou pas)
### Récupération
- Rajouter le genre sur les tracks récupérées
- Rajouter les albums sur lesquels sont la track
- Rajouter les artists qui ont participé à la track

## Ajout des validations de données (by Mister Gavin)
- Ajout de la librairie yup 
- Mise en place des modèles yup
- Mise en place d'un middleware pour valider les données du body
- Mise en place du middleware sur les routes appropriées
- Modification des modèles sequelize pour ajouter de la validation côté db également

## Mise en place du JWT 
- Installer jsonwebtoken dans l'app
- Créer les deux méthodes encode/decode (utils)
- Ajouter variables d'environnement Secret, Issuer, Audience
- Création du token dans login et register (controller)
- Mise en place du middleware qui va décoder le token (middleware + routes)

## Ajout du like des tracks par un user
- Création d'une nouvelle track route
- Création d'une méthode like dans track controller
- Création d'une méthode like dans track service
- Ajout du lien track-user dans le service

## Ajout du dislike

## Ajout d'une vérif role + userId sur user/getById
- Verifier à partir du token si role = Admin
- Si pas, vérifier à partir du token si l'id de la route = id de l'utilisateur qui fait la requête

## Mise en place de multer pour la gestion de fichiers
- rajouter le middleware static sur express
- https://www.npmjs.com/package/multer
- installer multer -> npm i multer
- Si on veut générer un uuid pour le nom de nos fichiers : installer uuid -> npm i uuid
- https://www.npmjs.com/package/uuid
- Configuration de multer dans la(les) route(s) + utilisation du middleware
- Récupération du fichier dans le controller
- Mise en place de l'uptdate cover dans le service

## Ajout d'un avatar à nos utilisateurs (Exo)
- Penser en plus à update user DTO puisqu'on n'avait pas prévu de renvoyer avatar

## Ajout des cors 
- https://www.npmjs.com/package/cors