const { Request, Response } = require('express');
const trackService = require('../services/track.service');
const { ErrorResponse } = require('../utils/error.response');
const { SuccessArrayResponse, SuccessResponse } = require('../utils/success.response');


const trackController = {

    /** 
     * Get All
     * @param {Request} req
     * @param {Response} res
     */
    getAll : async (req, res) => {
        const { offset, limit } = req.pagination;
        const { tracks , count } = await trackService.getAll(offset, limit);
        res.status(200).json(new SuccessArrayResponse( tracks , count)); 
    },

    /** 
     * Get By Id
     * @param {Request} req
     * @param {Response} res
     */
    getById : async (req, res) => {
        const { id } = req.params;
        const track = await trackService.getById(id);
        if(!track) {
            res.sendStatus(404);
            return;
        }
        res.status(200).json(new SuccessResponse(track));
    },

    /** 
     * Create
     * @param {Request} req
     * @param {Response} res
     */
    create : async (req, res) => {
        const data = req.body;
        const track = await trackService.create(data);
        res.location('/track/'+ track.id);
        res.status(201).json(new SuccessResponse(track, 201));
    },

    /** 
     * Update
     * @param {Request} req
     * @param {Response} res
     */
    update : async (req, res) => {
        const { id } = req.params;
        const data = req.body;

        const updated = await trackService.update(id, data);
        if(!updated) {
            res.sendStatus(404);
            return;
        }
        res.sendStatus(204);
    },

    /** 
     * Delete
     * @param {Request} req
     * @param {Response} res
     */
    delete : async (req, res) => {
        const { id } = req.params;
        const deleted = await trackService.delete(id);
        if(!deleted) {
            res.sendStatus(404);
            return;
        }
        res.sendStatus(204);
    },

     /** 
     * Like A Track
     * @param {Request} req
     * @param {Response} res
     */
    like : async (req, res) => {
        //res.sendStatus(501);

        // 1- Extraire l'id de la Track
        const trackId = req.params.id // const { id } = req.params
        // 2- Extraire l'id de l'utilisateur
        const userId = req.user.id // const { id } = req.user
        // 3- Appeler le service qui se charge de la logique
        const like = await trackService.like(trackId, userId)
        //On vérifié le résultat de like
        //Si null 404
        if(!like) {
            res.status(404).json(new ErrorResponse( "TrackId or UserId not found", 404))
            return
        }
        //Sinon, 201 - Created
        res.location('/track/'+ trackId) //Ajout du lien vers la track qui vient d'être likée dans la response
        res.status(201).json(new SuccessResponse({ msg : "Like success"}, 201))

        
    },

    dislike : async (req, res) => {
        //res.sendStatus(501);
        // 1- Extraire l'id de la Track
        const trackId = req.params.id // const { id } = req.params
        // 2- Extraire l'id de l'utilisateur
        const userId = req.user.id // const { id } = req.user
        // 3- Appeler le service qui se charge de la logique
        const dislike = await trackService.dislike(trackId, userId);
        if(!dislike) {
            res.status(404).json(new ErrorResponse( "TrackId or UserId not found or link is not present", 404))
            return
        }
        res.status(204).json(new SuccessResponse({ msg : "Dislike success"}, 204))

    }
}

module.exports = trackController;