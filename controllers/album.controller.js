const { Request, Response } = require('express');
const albumService = require('../services/album.service');
const { ErrorResponse } = require('../utils/error.response');
const { SuccessArrayResponse, SuccessResponse } = require('../utils/success.response');


const albumController = {

    /** 
     * Get All
     * @param {Request} req
     * @param {Response} res
     */
    getAll : async(req, res) => {
        const { offset, limit } = req.pagination;
        const { albums , count } = await albumService.getAll(offset, limit);
        res.status(200).json(new SuccessArrayResponse(albums, count));    },

    /** 
     * Get By Id
     * @param {Request} req
     * @param {Response} res
     */
    getById : async (req, res) => {
        const { id } = req.params;
        const album = await albumService.getById(id);
        if(!album) {
            res.sendStatus(404);
            return;
        }
        res.status(200).json(new SuccessResponse(album));
    },

    /** 
     * Create
     * @param {Request} req
     * @param {Response} res
     */
    create : async (req, res) => {
        const data = req.body;
        const album = await albumService.create(data);
        res.location('/album/'+ album.id);
        res.status(201).json(new SuccessResponse(album, 201));
    },

    /** 
     * Update
     * @param {Request} req
     * @param {Response} res
     */
    update : async (req, res) => {
        const { id } = req.params;
        const data = req.body;

        const updated = await albumService.update(id, data);
        if(!updated) {
            res.sendStatus(404);
            return;
        }
        res.location = '/album/'+id
        res.sendStatus(204);
    },

    /** 
     * UpdateCover
     * @param {Request} req
     * @param {Response} res
     */
    updateCover : async (req, res) => {
        //1 - Récupérer l'id de l'album
        const { id } = req.params
        //2 - Récupérer le nom du fichier
        //console.log(req.body); //Si on avait d'autres choses à modifier en plus de la cover -> body
        console.log("controller file : ", req.file);
        //Dans file, nous avons toutes les données du fichier, avec son ancien nom (originalname) et le nouveau nom (filename), généré par notre middleware multer
        const filename = req.file.filename
        //3 - Appeler le service en lui fournissant l'id et le nom du fichier
        const isUpdated = await albumService.updateCover(id, filename)
        if(!isUpdated) {
            res.status(404).json(new ErrorResponse("Album not found", 404))
            return
        }
        res.location = '/album/'+id
        res.status(204).json(new SuccessResponse({ msg : "Cover modifiée avec succès"}, 204))
        //res.sendStatus(501)
    },

    /** 
     * Delete
     * @param {Request} req
     * @param {Response} res
     */
    delete : async (req, res) => {
        const { id } = req.params;
        const deleted = await albumService.delete(id);
        if(!deleted) {
            res.sendStatus(404);
            return;
        }
        res.sendStatus(204);
    }
}

module.exports = albumController;