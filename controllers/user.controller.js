const { Request, Response } = require("express");
const userService = require("../services/user.service");
const { ErrorResponse } = require("../utils/error.response");
const { SuccessArrayResponse, SuccessResponse } = require("../utils/success.response");

const userController = {

    /**
     * Get All
     * @param {Request} req
     * @param {Response} res
     */
    getAll : async (req, res) => {
        const { offset, limit } = req.pagination;
        const { users, count } = await userService.getAll(offset, limit);
        res.status(200).json(new SuccessArrayResponse(users, count));
    },

    /**
     * Get By Id
     * @param {Request} req
     * @param {Response} res
     */
    getById : async (req, res) => {
        const { id } = req.params;

        //Vérification sur les autorisations de l'utilisateur
        //Le role Admin
        //On va le chercher dans le token : req.user.role
        const connectedUserRole = req.user.role
        //Les id -> dans le token : req.user.id
        const connectedUserId = req.user.id

        //S'il n'est ni Admin et si les id ne correspondent pas, on renvoie une erreur
        if(connectedUserRole !== "Admin" && connectedUserId !== parseInt(id) ) {
            //Acces interdit, il n'a pas le droit d'accéder au contenu
            res.status(403).json(new ErrorResponse( "Accès interdit, vous n'êtes ni Admin, ni l'utilisateur lié au profil" , 403))
            return
        }
    
        //Sinon, on fait la requête
        const user = await userService.getById(id);
        if(!user) {
            res.sendStatus(404);
            return;
        }
        res.status(200).json(new SuccessResponse(user));
    },

    /**
     * Update
     * @param {Request} req
     * @param {Response} res
     */
    update : async (req, res) => {
        const { id } = req.params;
        const data  = req.body;
       const updated = await userService.update(data, id);
       if(!updated) {
            res.sendStatus(404);
            return;
       }
       res.sendStatus(204); 
    },

    /**
     * Update Avatar
     * @param {Request} req
     * @param {Response} res
     */
    updateAvatar : async (req, res) => {
        //Récupération id
        const id = req.params.id
        //Récupération fichier
        const filename = req.file.filename

        const userId = req.user.id
        const userRole = req.user.role
        if(userRole !== 'Admin' && userId !== parseInt(id)) {
            res.status(403).json(new ErrorResponse( "Accès interdit, vous n'êtes ni Admin, ni l'utilisateur lié au profil" , 403))
        }

        const updated = await userService.updateAvatar(id, filename)
        if(!updated) {
            return res.status(404).json(new ErrorResponse("User not found", 404))
        }
        res.status(204).json(new SuccessResponse({msg : 'Avatar update success'}, 204))
        //res.sendStatus(501)

    },

    /**
     * Get All
     * @param {Request} req
     * @param {Response} res
     */
    delete : async (req, res) => {
        const { id } = req.params;
        const deleted = await userService.delete(id);
        if(!deleted) {
            res.sendStatus(404);
            return;
        }
        res.sendStatus(204); 
    }
}

module.exports = userController;